#/bin/bash

set -e
#eux

AGE=$1

DATUM=`date "+%Y-%m-%d"`

trap "rm /var/spool/ftn/textfiles/newfiles" EXIT
trap "rm /var/spool/ftn/textfiles/newfiles_*" EXIT
trap "rm /tmp/FILES*" EXIT
#trap "find /var/spool/ftn/filebase/ -exec chown ftn:ftn {} \+" EXIT

# sync Aminet mirror
/usr/bin/rsync -aB8192 --no-motd --chown=ftn:ftn --delete --exclude="*.bbs" rsync://vserv1.windfluechter.net/aminet  /var/spool/ftn/filebase/aminet/

# generate Aminet files.bbs 
/root/bin/aminet_filesbbs.sh

# post newfiles
/root/bin/fbase_newfiles.sh ${AGE}
if [ -s /var/spool/ftn/textfiles/newfiles_${AGE} ]; then 
	sudo -u ftn cat /var/spool/ftn/textfiles/newfiles.header /var/spool/ftn/textfiles/newfiles_${AGE} /var/spool/ftn/textfiles/newfiles.footer > /var/spool/ftn/textfiles/newfiles
else 
	rm /var/spool/ftn/textfiles/newfiles
	rm /var/spool/ftn/textfiles/newfiles_${AGE}
	echo "No new files today. Sorry!"
fi

if [ -s /var/spool/ftn/textfiles/newfiles ]; then 

	case ${AGE} in
		1)
			sudo -u ftn hpt post -x -nf "FileFix" -nt "All" -af "2:2452/413" -s "Neue Aminet Files in der AmigaXess (${DATUM})" -e "Announce.AX" /var/spool/ftn/textfiles/newfiles
			sudo -u ftn hpt post -x -nf "AmigaXess" -nt "All" -af "2:2452/413" -s "Neue Files in der AmigaXess (${DATUM})" -e "Announce.024" /var/spool/ftn/textfiles/newfiles
			sudo -u ftn hpt post -x -nf "AmigaXess" -nt "All" -af "21:1/110" -s "New Aminet Files available at AmigaXess (${DATUM})" -e "fsx_fil" /var/spool/ftn/textfiles/newfiles
			sudo -u ftn hpt post -x -nf "AmigaXess" -nt "All" -af "39:170/400" -s "New Aminet Files available at AmigaXess (${DATUM})" -e "File_Announce_Amy" /var/spool/ftn/textfiles/newfiles
			# hatch new files
			/root/bin/aminet_hatchnewfiles.sh 1
			;;
		7)
			sudo -u ftn hpt post -x -nf "FileFix" -nt "All" -af "2:2452/413" -s "Neue Aminet Files in der AmigaXess (7 Tage, ${DATUM})" -e "Announce.AX" /var/spool/ftn/textfiles/newfiles
			sudo -u ftn hpt post -x -nf "AmigaXess" -nt "All" -af "2:2452/413" -s "Neue Aminet Files in der AmigaXess (7 Tage)" -e "Announce.024" /var/spool/ftn/textfiles/newfiles
			sudo -u ftn hpt post -x -nf "AmigaXess" -nt "All" -af "21:1/110" -s "New Aminet Files available at AmigaXess (7 days, ${DATUM})" -e "fsx_fil" /var/spool/ftn/textfiles/newfiles
			sudo -u ftn hpt post -x -nf "AmigaXess" -nt "All" -af "39:170/400" -s "New Aminet Files available at AmigaXess (7 days)" -e "File_Announce_Amy" /var/spool/ftn/textfiles/newfiles
			# hatch new files of this day
			/root/bin/aminet_hatchnewfiles.sh 1
			;;
	esac
fi


# finally change user:group to belong to ftn:ftn for all files
find /var/spool/ftn/filebase/ -exec chown ftn:ftn {} \+

# ... and then export the echoposts... 
sudo -u ftn hpt scan 

# ... and compile the nodelists... 
sudo -u ftn nlupd | grep -v nlupdate

# invoke mfreq and generate filelist
sudo -u ftn mfreq-index
RC=$?
if [ ${RC} -gt 0 ]; then 
	echo "mfreq-index returned error code ${RC}!"
fi 	
RC=0
sudo -u ftn mfreq-list 
RC=$?
if [ ${RC} -gt 0 ]; then 
	echo "mfreq-list returned error code ${RC}!"
fi 	
RC=0

# combine fielist into a single one and add zip it...
cat /tmp/FILES /tmp/FILES_AMINET > /var/spool/ftn/filebase/fido/filelists/22452413.LST
zip -uqj /var/spool/ftn/filebase/fido/filelists/22452413.zip /var/spool/ftn/filebase/fido/filelists/22452413.LST



