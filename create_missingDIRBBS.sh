#!/bin/bash
set -e
OLDIFS=`echo $IFS`
#IFS=$(echo -en "\n\b")
IFS=$'\n'

PATHFILEBASE="/var/spool/ftn/filebase"

for d in `find ${PATHFILEBASE} -type d` ; do 
	DESC=`echo ${d} | sed -e 's/\/var\/spool\/ftn\/filebase\///g' `
	if [[ -e ${d}/dir.bbs ]] ; then 
		echo "" > /dev/null
	else
		sudo -u ftn echo "${DESC}" > ${d}/dir.bbs
	fi
done
