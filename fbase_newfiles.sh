#/bin/bash
set -e
#ux
AGE=$1

SPC=`printf "%40s" " "`
                                       
deltempfiles () {
	if [ -e /tmp/newdir.txt ]; then rm /tmp/newdir.txt ; fi
	if [ -e /tmp/newfiles.find.${AGE} ]; then rm /tmp/newfiles.find.${AGE} ; fi
	if [ -e /tmp/newfiles.grep ]; then rm /tmp/newfiles.grep ; fi
	if [ -e /tmp/newfiles.grep ]; then rm /tmp/newfiles.grep ; fi
	if [ -e /tmp/newfiles.out ]; then rm /tmp/newfiles.out ; fi
}

deltempfiles

find /var/spool/ftn/filebase/aminet -type f -mtime -${AGE} | grep -v ".*\.bbs$" | grep  -v -e "info\/adt" -v -e "aminet\/INDEX"  -v -e "aminet\/RECENT" > /tmp/newfiles.find.${AGE}
for f in `cat /tmp/newfiles.find.${AGE}`; do 
	dirname $f >> /tmp/newdir.txt 
done 
if [ -s /tmp/newdir.txt ]; then 
	cat /tmp/newdir.txt | uniq > /tmp/newdir2.txt
	mv /tmp/newdir2.txt /tmp/newdir.txt
else
	exit 1
fi


for dir in `cat /tmp/newdir.txt`; do 
	d=`echo ${dir} | sed -e 's/\/var\/spool\/ftn\/filebase\/aminet\///g'`
	for f in `find ${dir} -type f -mtime -${AGE} | grep -v ".*\.bbs$" | grep -v -e "info\/adt" -v -e "aminet\/INDEX"  -v -e "aminet\/RECENT" | sed -r 's/(.*)\..*/\1/'`; do
		filename=`basename ${f}`
		echo ${filename}  >> /tmp/newfiles.grep
	done
	cat /tmp/newfiles.grep | sort -u > /tmp/newfiles.grep2
	mv /tmp/newfiles.grep2 /tmp/newfiles.grep
	#printf "%0.s-" {1..78} >> /tmp/newfiles.out 
	echo "------------------------------------------------------------------------------" >> /tmp/newfiles.out 
	#echo "Aminet: ${d}" >> /tmp/newfiles.out 
	section=`echo "Filearea: aminet/${d}" | sed -e 's/\//\./g'`
	mirror="http://mirror.amigaxess.de/aminet/${d}"
	#len1=`echo $((72-${#mirror}/2+1))`
	len=`echo $((60-${#mirror}+${#section}))`
	#echo $len
	printf "%-1s %${len}s\n" "${section}" "${mirror}" >> /tmp/newfiles.out
	echo -n "Description: " >> /tmp/newfiles.out 
	cat ${dir}/dir.bbs >> /tmp/newfiles.out 
	#printf "%0.s-" {1..78} >> /tmp/newfiles.out 
	echo "------------------------------------------------------------------------------" >> /tmp/newfiles.out 
	grep -f /tmp/newfiles.grep ${dir}/files.bbs | sed 's/./&\n                                       /78' | fold -s -w 78 >> /tmp/newfiles.out
	echo >> /tmp/newfiles.out 
	echo >> /tmp/newfiles.out 
	#if [[ "${dir}" == "demo/euro" ]]; then
	#	exit 0
	#fi
	rm /tmp/newfiles.grep 
done

if [ -e /tmp/newfiles.out ]; then 
	cp /tmp/newfiles.out /var/spool/ftn/textfiles/newfiles_${AGE}
	chown -R ftn:ftn /var/spool/ftn/textfiles
fi

#deltempfiles
