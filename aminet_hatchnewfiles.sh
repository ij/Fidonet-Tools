#!/bin/bash

set -eu

AGE=$1

SPC=`printf "%40s" " "`
                                       
deltempfiles () {
	if [[ -e /tmp/hatchdir.txt ]]; then rm /tmp/hatchdir.txt ; fi
	if [[ -e /tmp/hatchfiles.find.${AGE} ]]; then rm /tmp/hatchfiles.find.${AGE} ; fi
	if [[ -e /tmp/hatchfiles.grep ]]; then rm /tmp/hatchfiles.grep ; fi
	if [[ -e /tmp/hatchfiles.grep ]]; then rm /tmp/hatchfiles.grep ; fi
	if [[ -e /tmp/hatchfiles.out ]]; then rm /tmp/hatchfiles.out ; fi
}

deltempfiles

find /var/spool/ftn/filebase/aminet -type f -mtime -${AGE} | grep -v ".*\.bbs$" | grep  -v -e "info\/adt" -v -e "aminet\/INDEX"  -v -e "aminet\/RECENT" > /tmp/hatchfiles.find.${AGE}
for f in `cat /tmp/hatchfiles.find.${AGE}`; do 
	dirname $f >> /tmp/hatchdir.txt 
done 
if [ -s /tmp/hatchdir.txt ]; then 
	cat /tmp/hatchdir.txt | uniq > /tmp/hatchdir..txt
	mv /tmp/hatchdir..txt /tmp/hatchdir.txt
else
	exit 1
fi


for dir in `cat /tmp/hatchdir.txt`; do 
	#d=`echo ${dir} | sed -e 's/\/var\/spool\/ftn\/filebase\/aminet\///g'`
	d=`echo ${dir} | sed -e 's/\/var\/spool\/ftn\/filebase\///g'`
	section=`echo $d | sed -e 's/\//./g'`
	for f in `find ${dir} -type f -mtime -${AGE} | grep -v ".*\.readme$" | grep -v ".*\.bbs$" | grep -v -e "info\/adt" -v -e "aminet\/INDEX"  -v -e "aminet\/RECENT"`; do
		filename=`basename ${f}`
		fn=`echo ${f} | sed -r 's/(.*)\..*/\1/'`
		if [ -e ${fn}.readme ] ; then
			DESC=`grep -m 1 -i "^Short:" ${fn}.readme | cut -d " " -f2- | tr -s " " | sed 's/^ *//g'`
			readme="${fn}.readme"
			echo "Section: ${section} - hatching ${filename} with readme" 
			sudo -u ftn htick hatch ${f} ${section} desc "${DESC}" @${readme}
		else 
			DESC=${filename}
			echo "Section: ${section} - hatching ${filename}" 
			sudo -u ftn htick hatch ${f} ${section} desc "${DESC}"	
		fi
		#echo ${filename}  >> /tmp/hatchfiles.grep
		#sudo -u ftn htick hatch /var/spool/ftn/filebase/aminet/gfx/show/vlc-0.8.6f-mos-b6-src.lha gfx:show desc "VLC Player for Morphos" @/var/spool/ftn/filebase/aminet/gfx/show/vlc-0.8.6f-mos-b6-src.readme
	done
done


