# Fidonet-Tools

Some tools for operating my Fidonet node. 

# Crontab 
Here you can see the crontab entries for daily announcements of new Aminet files and weekly announcement. The bi-weekly announcement is commented out. 
```
45 19    *   *  1-6     /root/bin/bbs_maintenance.sh 1
0 20     *   *   0     /root/bin/bbs_maintenance.sh 7
#0 23   1,14  *   0     /root/bin/bbs_maintenance.sh 14
```

# generate Aminet files.bbs 
/root/bin/aminet_filesbbs.sh

# post newfiles
/root/bin/fbase_newfiles.sh ${AGE}
