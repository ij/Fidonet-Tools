#!/bin/bash
set -e
OLDIFS=`echo $IFS`
#IFS=$(echo -en "\n\b")
IFS=$'\n'

FMT="%-28s %8d%-1s %.120s\n"

PATHAMINET="/var/spool/ftn/filebase/aminet"
TREEFILE="${PATHAMINET}/info/adt/tree"

# generate dir.bbs
cd ${PATHAMINET}
for line in `cat ${TREEFILE} | sed -e '/new/d'` ; do 
	#echo "${line}"
	DIRPATH=`echo "${line}" | awk '{print $1}'`
	#DIRDESC=`echo "${line}" | awk -F "\n" '{for(i=2;i<=NF;++i)print $i}' | sed -e 's/\n//g'`
	DIRDESC=`echo "${line}" | tr -s " " | cut -d " " -f 2- | sed -e 's/\n//g'`
	#echo ${line} | awk -F ":" '{for(i=2;i<=NF;++i)print $i}' > ${DIRPATH}/dir.bbs
	echo "${DIRDESC}" > ${PATHAMINET}/${DIRPATH}/dir.bbs
	OLDPWD=`pwd`
	cd ${PATHAMINET}/${DIRPATH}
	if [[ -f files.bbs ]] ; then 
		rm files.bbs
	fi
	#echo "${PATHAMINET}/${DIRPATH}"
	#echo ======================================
	#for f in `ls -I "*.bbs" -I "*.readme" --format='single-column'` ; do
	for l in `find ./ -type f -printf "%f;%s\n" | grep -v ".*\.bbs;[0-9.*]"` ; do
		#echo $l
		f=`echo ${l} | cut -d ";" -f1`
		#l2=`echo ${l} | cut -d ";" -f2`
		SIZE=`echo ${l} | cut -d ";" -f2`
		#SIZE=`echo $((l2/1024))`
		if [[ ${SIZE}/1024 -eq 0 ]]; then
			SIZE="1024"
			UNIT="k"
		fi
		#elif [[ "$SIZE" -lt 1048576 ]]; then
			SIZE=`echo $((SIZE/1024))`
			UNIT="k"
		#elif [[ "$SIZE" -gt 1048576 ]]; then
		#	SIZE=`echo $((SIZE/1024/1024))`
		#	UNIT="M"
		#elif [[ "$SIZE" = 1073741824 ]]; then
		#	SIZE=`echo $((SIZE/1024/1024/1024))`
		#	UNIT="G"
		#fi 
		if [[ -d ${f} ]] ; then 
			echo "" > /dev/null
		else
			if [[ "${f}" =~ .*readme.* ]]; then
				fn=`echo ${f} | sed -r 's/(.*)\..*/\1/'`
				if [[ ${#f} -gt 27 ]]; then 
					f="${f:0:27}*"
				fi
				#printf "${FMT}" "${f}" "${SIZE}" "Readme for ${fn}" #>> files.bbs
				printf "${FMT}" "${f}" "${SIZE}" "${UNIT}" "Readme for ${fn} - http://aminet.net/${DIRPATH}/${f}" >> files.bbs
			elif [[ -f ${f} ]] ; then 
				#echo "File: ${f}"
				#fn=`echo ${f} | awk -F"." '{print $1}'`
				fn=`echo ${f} | sed -r 's/(.*)\..*/\1/'`
				if [[ ${#f} -gt 27 ]]; then 
					f="${f:0:27}*"
				fi
				if [ -e ${fn}.readme ] ; then 
					#DESC=`cat ${fn}.readme | awk -v RS="" -v ORS="\n\n" '/Short:/' | grep -i "Short:" | tr -s " " | cut -d " " -f2- ` 
					#ARCH=`cat ${fn}.readme | awk -v RS="" -v ORS="\n\n" '/Short:/' | grep -i "Architecture:" | tr -s " " | cut -d " " -f2-`
					DESC=`grep -m 1 -i "^Short:" ${fn}.readme | cut -d " " -f2- | tr -s " " | sed 's/^ *//g'` 
					ARCH=`grep -m 1 -i "^Architecture:" ${fn}.readme | cut -d " " -f2- | tr -s " " | sed 's/^ *//g'`
					if [[ -z "${ARCH}" ]]; then 
						#echo $l
						#echo "${f} : ${SIZE} ${DESC} '${ARCH}'" #>> files.bbs
						#echo "${FMT}" "${f}" "${SIZE}" "${DESC}" #>> files.bbs
						#printf "${FMT}" "${f}" "${SIZE}" "${DESC}" #>> files.bbs
						printf "${FMT}" "${f}" "${SIZE}" "${UNIT}" "${DESC}" >> files.bbs
					elif [[ -n "${ARCH}"  ]]; then
						#echo "${f} : ${DESC}" >> files.bbs
						#printf "${FMT}" "${f}" "${SIZE}" "${DESC} (${ARCH})" #>> files.bbs
						printf "${FMT}" "${f}" "${SIZE}" "${UNIT}" "${DESC} (${ARCH})" >> files.bbs
					fi
				else
					#echo "${f} : No Readme available" >> files.bbs
					#printf "${FMT}" "${f}" "${SIZE}" "No Readme available" #>> files.bbs
					printf "${FMT}" "${f}" "${SIZE}" "${UNIT}" "No Readme available" >> files.bbs
				fi
				unset ARCH
				unset DESC
			fi
		fi
		unset SIZE
		#echo "${f} - ${fn} - ${SIZE}k - ${DESC}"
	done
	#chown ftn:ftn /dev/shm/files.bbs
	#mv /dev/shm/files.bbs ${PATHAMINET}/${DIRPATH}	
done

